package pages;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import utils.DataBaseManager;
import utils.Log4Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SmartphonesPage extends BasePage {
    public static final String PAGE_NUMBER_LOCATOR_PATTERN = ".//ul[@name = 'paginator']//a[text() = '%s']";

    @FindBy(xpath = ".//i[@class = 'g-tag g-tag-icon-middle-popularity sprite']/following-sibling::a")
    private List<WebElement> topSales;

    public SmartphonesPage(WebDriver driver) {
        super(driver);
    }

    public void navigateToPage(final int pageNumber) {
        String pageNumberLocator = String.format(PAGE_NUMBER_LOCATOR_PATTERN, pageNumber);
        WebElement nextPage = getDriver().findElement(By.xpath(pageNumberLocator));
        new Actions(getDriver()).moveToElement(nextPage).click().perform();
        Log4Test.info(String.format("Select %s page", pageNumber));
    }

    public void selectAllTopSales(){
        DataBaseManager dataBaseManager = new DataBaseManager();
        Map<String, String> topSaleSmartphones = new HashMap<String, String>();
        //String topSalesLocator = ".//i[@class = 'g-tag g-tag-icon-middle-popularity sprite']/following-sibling::a";
        List<WebElement> allPhones = getDriver().findElements(By.xpath(".//div[@class = 'g-i-tile-i-box-desc']"));
        for(WebElement element: allPhones){
            try {
                element.findElement(By.xpath(".//i[@class = 'g-tag g-tag-icon-middle-popularity sprite']/following-sibling::a"));
                String name = element.findElement(By.xpath(".//img")).getAttribute("title");
                System.out.println(name);
                String price = element.findElement(By.xpath(".//div[@class = 'g-price-uah']")).getText();
                System.out.println(price);
                topSaleSmartphones.put(name, price);
            } catch (NoSuchElementException e) {

            }
        }
        dataBaseManager.insertAllSmartphones(topSaleSmartphones);
    }
}
