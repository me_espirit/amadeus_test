package pages;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Log4Test;

public class PhonesPage extends BasePage{

    @FindBy(linkText = "Смартфоны")
    private WebElement smartphones;

    public PhonesPage(WebDriver driver) {
        super(driver);
    }

    public void selectSmartphones(){
        getDriver().findElement(By.linkText("Смартфоны")).click();
        Log4Test.info("Click on Smartphones");
        //smartphones.click();
    }


}
