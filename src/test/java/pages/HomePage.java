package pages;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Log4Test;

public class HomePage extends BasePage{

    @FindBy(xpath = ".//a[@data-title='Телефоны, ТВ и электроника']")
    private WebElement electronics;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void selectElectronics(){
        Log4Test.info("Wait page loading");
        super.waitForPageLoaded(getDriver());
        getDriver().findElement(By.xpath(".//a[@data-title='Телефоны, ТВ и электроника']")).click();
        Log4Test.info("Click on Electronics");
        //electronics.click();
    }
}
