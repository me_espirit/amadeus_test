package pages;

import core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Log4Test;

public class ElectronicsPage  extends BasePage{

    @FindBy(linkText = "Телефоны")
    private WebElement phones;

    public ElectronicsPage(WebDriver driver) {
        super(driver);
    }

    public void selectPhones(){
        getDriver().findElement(By.linkText("Телефоны")).click();
        Log4Test.info("Click on Phones");
        //phones.click();
    }
}
