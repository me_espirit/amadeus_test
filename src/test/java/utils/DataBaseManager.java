package utils;

import java.util.Map;

public class DataBaseManager {
    private static final  String TABLE = "rozetka_top_smartphones";
    private static final  String NAME_COL = "name";
    private static final  String PRICE_COL = "price";

    DBConnector connector;

    private void insertSmartphone(String name, String price){
        connector = new DBConnector();
        String query = String.format("INSERT INTO %s (%s, %s) VALUES(%s, %s);", TABLE, NAME_COL, PRICE_COL, name, price);
        connector.insertStatement(query);
    }

    private void selectAllSmartphones(){
        connector = new DBConnector();
        String query = String.format("SELECT %s, %s FROM %s;", NAME_COL, PRICE_COL, TABLE);
        connector.selectStatement(query);
    }

    public void insertAllSmartphones(Map<String, String> smartphones){
        for(Map.Entry<String, String> entry: smartphones.entrySet()){
            insertSmartphone(entry.getKey(), entry.getValue());
        }
    }

}
