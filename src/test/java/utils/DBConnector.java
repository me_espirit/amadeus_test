package utils;

import java.sql.*;

public class DBConnector {
    private static final  String SERVER = "localhost";
    private static final  String USER = "root";
    private static final  String PASS = "****";
    private static final  String DATABASE = "test";

    private Connection dbconn = null;
    private Statement stmt = null;
    private ResultSet rs = null;

    private Connection getConnection(){
        if(dbconn == null){
            try {
                dbconn = DriverManager.getConnection("jdbc:mysql://" + SERVER +"/" + DATABASE, USER, PASS);
                return dbconn;
            }catch (SQLException ex){
                Log4Test.error("SQLException: " + ex.getMessage());
                Log4Test.error("SQLState: " + ex.getSQLState());
                Log4Test.error("VendorError: " + ex.getErrorCode());
            }
        }
        return this.dbconn;
    }

    private void closeConnection(){
        try {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (dbconn != null) {
                dbconn.close();
            }
        } catch (Exception ex) {
            Log4Test.error("SQLException: " + ex.getMessage());
        }
    }

    public void selectStatement(String query){
        dbconn = getConnection();

        try {
            stmt = dbconn.createStatement();
            rs = stmt.executeQuery(query);

        }catch (Exception ex){
            Log4Test.error("SQLException: " + ex.getMessage());
        }
    }

    public void insertStatement(String query){
        dbconn = getConnection();

        try {
            stmt = dbconn.createStatement();
            stmt.executeUpdate(query);

        }catch (Exception ex){
            Log4Test.error("SQLException: " + ex.getMessage());
        }

    }

}
