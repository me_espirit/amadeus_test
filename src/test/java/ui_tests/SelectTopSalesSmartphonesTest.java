package ui_tests;

import core.TestBase;
import org.testng.annotations.Test;
import pages.ElectronicsPage;
import pages.HomePage;
import pages.PhonesPage;
import pages.SmartphonesPage;

import static org.assertj.core.api.Assertions.assertThat;

public class SelectTopSalesSmartphonesTest extends TestBase {

    public final static String SITE_URL = "http://rozetka.com.ua/";

    @Test
    public void selectTopSalesSmartphonesTest() {
        HomePage homePage = new HomePage(getDriver());
        homePage.open(SITE_URL);
        assertThat(getDriver().getCurrentUrl()).as("").isEqualTo(SITE_URL);
        homePage.selectElectronics();

        ElectronicsPage electronicsPage = new ElectronicsPage(getDriver());
        electronicsPage.selectPhones();

        PhonesPage phonesPage = new PhonesPage(getDriver());
        phonesPage.selectSmartphones();

        SmartphonesPage smartphonesPage = new SmartphonesPage(getDriver());
        smartphonesPage.selectAllTopSales();
        smartphonesPage.navigateToPage(2);
        smartphonesPage.selectAllTopSales();
        smartphonesPage.navigateToPage(3);
        smartphonesPage.selectAllTopSales();
        assertThat(getDriver().getTitle()).as("Incorrect page title").contains("Страница 3");
    }

}
